#!/usr/bin/env python3
"""Automatic renove library
    NEED    community/python-beautifulsoup4 
                // Para parsear html
                // http://www.crummy.com/software/BeautifulSoup/bs4/doc/
            community/python-requests
                // Para hacer los post la pagina web
                // http://docs.python-requests.org/en/latest/
            community/dzen2 
                // Para notificar, mirar
                // https://wiki.archlinux.org/index.php/Dzen
------------------------------------------------------------
La primera implementacio que queda pendent es el fet de no tenir la contrasenya
en text pla.
Despues es podria implementar un ficher forms en json,
https://docs.python.org/3/library/json.html
amb diferents trucades i
generalitzar renove per que ho pugui fer per qualsevol pagina, yo tinc la de la
biblioteca de terrassa, 
Ara nomes pots llegir, diguem i et dono permisos perque faigis un fork de
experimental.
S'hauria deliminar aquesta miniwiki i nomes documentar en angles i ben fet. 
Per que funcioni nomes has de instalar el que et falti, el mail del repositori
aritmeeul@gmail.com 
Envian el mail del teu usuari de bitbucket i et dono permisos 
"""
from os import environ# {{{
from re import compile,search
from subprocess import Popen, PIPE, DEVNULL
from datetime import date, datetime
from requests import Session, Request, Response
from bs4 import BeautifulSoup# }}}

def renove(name, passwd):# {{{
    """
    Login and post in this url.
    @param name: Name for login.
    @param passwd: Passwd for login. 
    @return: Html of last page for notify.
    """
    url = "https://cataleg.ub.edu/patroninfo*cat"

    login = { 
            "extpatid" : name,
            "extpatpw" : passwd,
            "submit" : "submit",
            }
    parm1 = { 
            "checkoutpagecmd" : "requestRenewAll",
            "checkout_form" : "submit"
            }
    parm2 = { "renewall" : "submit" }

    s = Session()
    r = s.post(url, login, verify=False)
    r = s.post(r.url, parm1, verify=False)
    r = s.post(r.url, parm2, verify=False)

    return r.text# }}}

def notify(html):# {{{
    """
    Filter for notify in dzen2. 
    @param html: html strings.
    @return: number of lines needed, string.
    """
    out = ""
    index = 0
    soup = BeautifulSoup(html)
    rflag = compile("RESERVA")
    rdate = compile(r'[0-3][0-9]-[01][0-9]-[0-9]{2}')
    
    for (d,n) in [ ( rdate.search(j.find("td", class_="patFuncStatus").get_text()),
        j.find("span", class_="patFuncTitleMain").get_text() )
        for j in [ i for i in soup.find_all("tr",class_="patFuncEntry") ] 
        if rflag.search(j.find("td",class_="patFuncStatus").get_text()) ]:

        dates = d.group(0).split("-")
        today = datetime.today()
        da = datetime(int(dates[2])+2000,int(dates[1]),int(dates[0]),
                hour=0,minute=0,second=0)

        out += n+"\n"
        if today < da:
            out += "Faltan"+str(da-today)+"para entregarlo el"+str(da.date())+"\n"
        else:
            out += "Tenias que haberlo entregado hace: "+str(today-da)+" el"\
                    +str(da.date().strftime("%A %d. %B %Y"))+"\n"
        index += 2
    return index, out
# }}}

def decrypt():# {{{
    """GPGME"""
    
    gpgkey = environ.get('GPGKEY')
   
    assert gpgkey, "GPGKEY none"
    ctx = gpgme.Context()
    ctx.armor = True
    key = ctx.get_key(environ.get(gpgkey))
       
    f = open("rb", "forms")
    ctx.decrypt_verify([key],0,f,out)
    return out# }}}

if __name__ == '__main__':# {{{
    # falta implementar un modul gpg per no tindre que ficar les credencial en
    # text pla. La seguent linea es el main, out es una ( numero de linees ,
    # text ) 

    # name = your user for crai
    # passwd = your passwd for crai
    # out = notify(renove(name, passwd))
    if out[0] > 0: 
        try:
            p = Popen(['dzen2','-p','-x','0','-y','768','-w','600','-l',
                str(out[0]),'-sa','r','-ta','r','-u','-title-name','popup_sysinfo','-e',
                "onstart=uncollapse;button1=exit;button3=exit"],
                stdin=PIPE,stdout=DEVNULL,stderr=DEVNULL)
            p.communicate(input=bytes(out[1], 'UTF-8'))
        except OSError as err:
            print("Need install dzen2 for notify".format(err))# }}}
